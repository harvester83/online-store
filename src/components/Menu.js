import React from 'react';
import { Menu } from 'semantic-ui-react'

const MenuComponents = ({ totalPrice, count }) => (
    <Menu>
        <Menu.Item name='browse'
                   //onClick={this.handleItemClick}
        >
            Магазин книг
        </Menu.Item>

        <Menu.Menu position='right'>

            <Menu.Item name='signup' //onClick={this.handleItemClick}
            >
                Итого: &nbsp; <b>{totalPrice}</b> руб.
            </Menu.Item>

            <Menu.Item name='help' // onClick={this.handleItemClick}
            >
                Корзина: &nbsp; (<b>{count}</b>)руб.
            </Menu.Item>
        </Menu.Menu>
    </Menu>
);

export default MenuComponents;
