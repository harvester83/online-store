import React, { Component } from 'react';
import { Menu, Input } from 'semantic-ui-react';

const Filter = ({setFilter, filterBy, searchQuery, setSearchQuery}) => {
    return (
        <Menu secondary>
            <Menu.Item
                active={filterBy === 'all'}
                onClick={setFilter.bind(this, 'all')}
            >
                Все
            </Menu.Item>

            <Menu.Item
                active={filterBy === 'price_high'}
                onClick={setFilter.bind(this, 'price_high')}
            >
                По цене (высокой)
            </Menu.Item>

            <Menu.Item
                active={filterBy === 'price_low'}
                onClick={setFilter.bind(this, 'price_low')}
            >
                По цене (низкий)
            </Menu.Item>

            <Menu.Item
                active={filterBy === 'author'}
                onClick={setFilter.bind(this, 'author')}
            >
               По автору
            </Menu.Item>

            <Menu.Item>
                <Input
                   onChange={e => setSearchQuery(e.target.value)}
                   value={searchQuery}
                   placeholder="Введите запрос..."
                   icon="search" />
            </Menu.Item>
        </Menu>
    )
};

export default Filter;
