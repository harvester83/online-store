import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import rootReducer from './reducers';


import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

export default () => {
    const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(logger, thunk)));

    return store;
};

