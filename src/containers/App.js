import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import * as booksActions from "../actions/books";
import * as filterActions from "../actions/filter";
import App from '../components/App'
import orderBy from 'lodash/orderBy'

const sortBy = (books, filterBy) => {
   switch (filterBy) {
       case 'price_high':
           return orderBy(books, 'price', 'desc');

       case 'price_low':
           return orderBy(books, 'price', 'asc');

       case 'author':
           return orderBy(books, 'author', 'asc');

       default:
           return books;
   }
};

const filterBooks = (books, searchQuery) =>
    books.filter(
      obj =>
          obj.title.toLowerCase().indexOf(searchQuery.toLowerCase()) >= 0 ||
          obj.author.toLowerCase().indexOf(searchQuery.toLowerCase()) >= 0
    );

const searchBooks = (books, filterBy, searchQuery) => {
    return sortBy(filterBooks(books, searchQuery), filterBy)
};

const mapSateToStore = ({books, filter}) => ({
    books:
        books.items &&
        searchBooks(books.items, filter.filterBy, filter.searchQuery),
    isReady: books.isReady
});

const mapDispatchToProps = dispatch => ({
    ...bindActionCreators(booksActions, dispatch),
    ...bindActionCreators(filterActions, dispatch)
});

export default connect(mapSateToStore, mapDispatchToProps)(App);
